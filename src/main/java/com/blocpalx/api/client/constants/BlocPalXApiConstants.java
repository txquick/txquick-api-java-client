package com.blocpalx.api.client.constants;

import org.apache.commons.lang3.builder.ToStringStyle;

public class BlocPalXApiConstants {

    /**
     * REST API Base URL
     */
    // LOCAL DEV
    public static final String LOCAL_API_BASE_URL = "http://127.0.0.1:8083/";
    // SANDBOX
    public static final String SANDBOX_API_BASE_URL = "https://api-sandbox.blocpal.com";
    // PRODUCTION
    public static final String PRODUCTION_API_BASE_URL = "https://api.blocpal.com";

    /**
     * Streaming API base URL.
     */
    // LOCAL DEV
    public static final String LOCAL_WS_API_BASE_URL = "ws://127.0.0.1:8083/ws";
    // SANDBOX
    public static final String SANDBOX_WS_API_BASE_URL = "wss://api-sandbox.blocpal.com/ws";
    // PRODUCTION
    public static final String PRODUCTION_WS_API_BASE_URL = "wss://api.blocpal.com/ws";

    /**
     * HTTP Header to be used for API-KEY authentication.
     */
    public static final String API_KEY_HEADER = "X-MBX-APIKEY";

    /**
     * Decorator to indicate that an endpoint requires an API key.
     */
    public static final String ENDPOINT_SECURITY_TYPE_APIKEY = "APIKEY";
    public static final String ENDPOINT_SECURITY_TYPE_APIKEY_HEADER = ENDPOINT_SECURITY_TYPE_APIKEY + ": #";

    /**
     * Decorator to indicate that an endpoint requires a signature.
     */
    public static final String ENDPOINT_SECURITY_TYPE_SIGNED = "SIGNED";
    public static final String ENDPOINT_SECURITY_TYPE_SIGNED_HEADER = ENDPOINT_SECURITY_TYPE_SIGNED + ": #";

    /**
     * Default receiving window.
     */
    public static final long DEFAULT_RECEIVING_WINDOW = 60_000L;

    /**
     * Default ToStringStyle used by toString methods.
     * Override this to change the output format of the overridden toString methods.
     * - Example ToStringStyle.JSON_STYLE
     */
    public static ToStringStyle TO_STRING_BUILDER_STYLE = ToStringStyle.SHORT_PREFIX_STYLE;

    /**
     * console log raw server responses.
     */
    public static final boolean OUTPUT_SERVER_RESPONSES = false;

}
