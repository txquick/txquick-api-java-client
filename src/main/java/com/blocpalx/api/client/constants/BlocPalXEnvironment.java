package com.blocpalx.api.client.constants;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Exchange trade engine environment
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public enum BlocPalXEnvironment {
    LOCAL,
    SANDBOX,
    PRODUCTION
}
