package com.blocpalx.api.client;

import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * BlocPalX API error object.
 */
public class BlocPalXApiError {

    /**
     * Error code.
     */
    private int code;

    /**
     * Error message.
     */
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, BlocPalXApiConstants.TO_STRING_BUILDER_STYLE)
                .append("code", code)
                .append("msg", msg)
                .toString();
    }
}
