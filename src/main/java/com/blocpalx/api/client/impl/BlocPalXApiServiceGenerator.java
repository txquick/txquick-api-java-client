package com.blocpalx.api.client.impl;

import com.blocpalx.api.client.BlocPalXApiError;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;

import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.exception.BlocPalXApiException;
import com.blocpalx.api.client.security.AuthenticationInterceptor;
import org.apache.commons.lang3.StringUtils;

import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

public class BlocPalXApiServiceGenerator {

    private static final OkHttpClient sharedClient;
    private static final Converter.Factory converterFactory = JacksonConverterFactory.create();

    static {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequestsPerHost(500);
        dispatcher.setMaxRequests(500);
        sharedClient = new OkHttpClient.Builder()
                .dispatcher(dispatcher)
                .connectTimeout(10, TimeUnit.SECONDS) // connect timeout
                .writeTimeout(65, TimeUnit.SECONDS) // write timeout
                .readTimeout(65, TimeUnit.SECONDS) // read timeout
                .pingInterval(20, TimeUnit.SECONDS)
                .build();
    }

    private static final Converter<ResponseBody, BlocPalXApiError> errorBodyConverter =
            (Converter<ResponseBody, BlocPalXApiError>) converterFactory.responseBodyConverter(
                    BlocPalXApiError.class, new Annotation[0], null);

    public static String getBaseUrlByEnvironment(BlocPalXEnvironment env) {
        switch (env) {
            case LOCAL:
                return BlocPalXApiConstants.LOCAL_API_BASE_URL;
            case SANDBOX:
                return BlocPalXApiConstants.SANDBOX_API_BASE_URL;
            case PRODUCTION:
                return BlocPalXApiConstants.PRODUCTION_API_BASE_URL;
        }
        return "";
    }

    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, BlocPalXEnvironment.PRODUCTION, null, null);
    }

    public static <S> S createService(Class<S> serviceClass, BlocPalXEnvironment env, String apiKey, String secret) {
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(getBaseUrlByEnvironment(env))
                .addConverterFactory(converterFactory);

        if (StringUtils.isEmpty(apiKey)) {
            retrofitBuilder.client(sharedClient);
        } else {
            // `adaptedClient` will use its own interceptor, but share thread pool etc with the 'parent' client
            AuthenticationInterceptor interceptor = new AuthenticationInterceptor(apiKey, secret);
            OkHttpClient adaptedClient = sharedClient.newBuilder().addInterceptor(interceptor).build();
            retrofitBuilder.client(adaptedClient);
        }

        Retrofit retrofit = retrofitBuilder.build();
        return retrofit.create(serviceClass);
    }

    /**
     * Execute a REST call and block until the response is received.
     */
    public static <T> T executeSync(Call<T> call) {
        try {
            if (BlocPalXApiConstants.OUTPUT_SERVER_RESPONSES) {
                System.out.println("Execute REST SYNC request: " + call.request());
            }
            Response<T> response = call.execute();
            if (BlocPalXApiConstants.OUTPUT_SERVER_RESPONSES) {
                System.out.println("  received response: " + response.raw());
            }
            if (response.isSuccessful()) {
                return response.body();
            } else {
                BlocPalXApiError apiError = getBlocPalXApiError(response);
                //eb remove
                System.out.println("  error: " + apiError);
                throw new BlocPalXApiException(apiError);
            }
        } catch (IOException e) {
            throw new BlocPalXApiException(e);
        }
    }

    /**
     * Extracts and converts the response error body into an object.
     */
    public static BlocPalXApiError getBlocPalXApiError(Response<?> response) throws IOException, BlocPalXApiException {
        return errorBodyConverter.convert(response.errorBody());
    }

    /**
     * Returns the shared OkHttpClient instance.
     */
    public static OkHttpClient getSharedClient() {
        return sharedClient;
    }

}
