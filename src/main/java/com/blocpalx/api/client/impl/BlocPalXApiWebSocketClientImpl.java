package com.blocpalx.api.client.impl;

import com.blocpalx.api.client.BlocPalXApiCallback;
import com.blocpalx.api.client.BlocPalXApiWebSocketClient;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.event.*;
import com.blocpalx.api.client.domain.market.CandlestickInterval;
import com.fasterxml.jackson.core.type.TypeReference;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

import java.io.Closeable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * BlocPalX API WebSocket client implementation using OkHttp.
 */
public class BlocPalXApiWebSocketClientImpl implements BlocPalXApiWebSocketClient, Closeable {

    private final OkHttpClient client;
    private final BlocPalXEnvironment env;

    public BlocPalXApiWebSocketClientImpl(BlocPalXEnvironment env, OkHttpClient client) {
        this.env = env;
        this.client = client;
    }

    @Override
    public Closeable onDiffDepthEvent(String symbols, BlocPalXApiCallback<DiffDepthEvent> callback) {
        final String channel = Arrays.stream(symbols.split(","))
                .map(String::trim)
                .map(s -> String.format("%s@depth", s))
                .collect(Collectors.joining("/"));
        return createNewWebSocket(channel, new BlocPalXApiWebSocketListener<>(callback, DiffDepthEvent.class));
    }

    @Override
    public Closeable onPartialDepthEvent(String symbols, int levels, BlocPalXApiCallback<PartialDepthEvent> callback) {
        if (levels != 5 && levels != 10 && levels != 20) levels = 5;
        final int lambdaLevels = levels;
        final String channel = Arrays.stream(symbols.split(","))
                .map(String::trim)
                .map(s -> String.format("%s@depth" + String.valueOf(lambdaLevels), s))
                .collect(Collectors.joining("/"));
        return createNewWebSocket(channel, new BlocPalXApiWebSocketListener<>(callback, PartialDepthEvent.class));
    }

    @Override
    public Closeable onCandlestickEvent(String symbols, CandlestickInterval interval, BlocPalXApiCallback<CandlestickEvent> callback) {
        final String channel = Arrays.stream(symbols.split(","))
                .map(String::trim)
                .map(s -> String.format("%s@kline_%s", s, interval.getIntervalId()))
                .collect(Collectors.joining("/"));
        return createNewWebSocket(channel, new BlocPalXApiWebSocketListener<>(callback, CandlestickEvent.class));
    }

    public Closeable onAggTradeEvent(String symbols, BlocPalXApiCallback<AggTradeEvent> callback) {
        final String channel = Arrays.stream(symbols.split(","))
                .map(String::trim)
                .map(s -> String.format("%s@aggTrade", s))
                .collect(Collectors.joining("/"));
        return createNewWebSocket(channel, new BlocPalXApiWebSocketListener<>(callback, AggTradeEvent.class));
    }

    public Closeable onUserDataUpdateEvent(String listenKey, BlocPalXApiCallback<UserDataUpdateEvent> callback) {
        return createNewWebSocket(listenKey, new BlocPalXApiWebSocketListener<>(callback, UserDataUpdateEvent.class));
    }

    @Override
    public Closeable onTickerEvent(String symbols, BlocPalXApiCallback<TickerEvent> callback) {
        final String channel = Arrays.stream(symbols.split(","))
                .map(String::trim)
                .map(s -> String.format("%s@ticker", s))
                .collect(Collectors.joining("/"));
        return createNewWebSocket(channel, new BlocPalXApiWebSocketListener<>(callback, TickerEvent.class));
    }

    public Closeable onAllMarketTickersEvent(BlocPalXApiCallback<List<TickerEvent>> callback) {
        final String channel = "!ticker@arr";
        return createNewWebSocket(channel, new BlocPalXApiWebSocketListener<>(callback, new TypeReference<List<TickerEvent>>() {
        }));
    }

    @Override
    public Closeable onBookTickerEvent(String symbols, BlocPalXApiCallback<BookTickerEvent> callback) {
        final String channel = Arrays.stream(symbols.split(","))
                .map(String::trim)
                .map(s -> String.format("%s@bookTicker", s))
                .collect(Collectors.joining("/"));
        return createNewWebSocket(channel, new BlocPalXApiWebSocketListener<>(callback, BookTickerEvent.class));
    }

    public Closeable onAllBookTickersEvent(BlocPalXApiCallback<BookTickerEvent> callback) {
        final String channel = "!bookTicker";
        return createNewWebSocket(channel, new BlocPalXApiWebSocketListener<>(callback, BookTickerEvent.class));
    }

    /**
     * @deprecated This method is no longer functional. Please use the returned {@link Closeable} from any of the other methods to close the web socket.
     */
    @Override
    public void close() {
    }

    @Override
    public String getBaseUrl() {
        String baseUrl = "";
        switch (env) {
            case LOCAL:
                baseUrl = BlocPalXApiConstants.LOCAL_WS_API_BASE_URL;
                break;
            case SANDBOX:
                baseUrl = BlocPalXApiConstants.SANDBOX_WS_API_BASE_URL;
                break;
            case PRODUCTION:
                baseUrl = BlocPalXApiConstants.PRODUCTION_WS_API_BASE_URL;
                break;
        }
        return baseUrl;
    }

    private Closeable createNewWebSocket(String channel, BlocPalXApiWebSocketListener<?> listener) {
        String streamingUrl = String.format("%s/%s", getBaseUrl(), channel);
        Request request = new Request.Builder().url(streamingUrl).build();
        final WebSocket webSocket = client.newWebSocket(request, listener);
        return () -> {
            final int code = 1000;
            listener.onClosing(webSocket, code, "closing");
            webSocket.close(code, null);
            listener.onClosed(webSocket, code, "closed");
        };
    }
}
