package com.blocpalx.api.client.impl;

import com.blocpalx.api.client.BlocPalXApiAsyncRestClient;
import com.blocpalx.api.client.BlocPalXApiCallback;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.account.*;
import com.blocpalx.api.client.domain.account.request.*;
import com.blocpalx.api.client.domain.event.ListenKey;
import com.blocpalx.api.client.domain.general.ExchangeInfo;
import com.blocpalx.api.client.domain.general.ServerTime;
import com.blocpalx.api.client.domain.general.UserAsset;
import com.blocpalx.api.client.domain.market.*;
import com.blocpalx.api.client.domain.market.request.OrderBookDepthLimit;

import java.util.List;

import static com.blocpalx.api.client.impl.BlocPalXApiServiceGenerator.createService;
import static com.blocpalx.api.client.impl.BlocPalXApiServiceGenerator.getBaseUrlByEnvironment;

/**
 * Implementation of the BlocPalX REST API using Retrofit with asynchronous/non-blocking method calls.
 */
public class BlocPalXApiAsyncRestClientImpl implements BlocPalXApiAsyncRestClient {

    private final BlocPalXApiService apiService;
    private final String baseUrl;

    public BlocPalXApiAsyncRestClientImpl(BlocPalXEnvironment env, String apiKey, String secret) {
        apiService = createService(BlocPalXApiService.class, env, apiKey, secret);
        baseUrl = getBaseUrlByEnvironment(env);
    }

    // General endpoints

    @Override
    public void ping(BlocPalXApiCallback<Void> callback) {
        apiService.ping().enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getServerTime(BlocPalXApiCallback<ServerTime> callback) {
        apiService.getServerTime().enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getExchangeInfo(BlocPalXApiCallback<ExchangeInfo> callback) {
        apiService.getExchangeInfo().enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    // Market Data endpoints

    @Override
    public void getOrderBook(String symbol, OrderBookDepthLimit limit, BlocPalXApiCallback<OrderBook> callback) {
        apiService.getOrderBook(symbol, limit.getLimit()).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getTrades(String symbol, Integer limit, BlocPalXApiCallback<List<TradeHistoryItem>> callback) {
        apiService.getTrades(symbol, limit).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getHistoricalTrades(String symbol, Integer limit, Long fromId, BlocPalXApiCallback<List<TradeHistoryItem>> callback) {
        apiService.getHistoricalTrades(symbol, limit, fromId).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getAggTrades(String symbol, String fromId, Integer limit, Long startTime, Long endTime, BlocPalXApiCallback<List<AggTrade>> callback) {
        apiService.getAggTrades(symbol, fromId, limit, startTime, endTime).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getAggTrades(String symbol, BlocPalXApiCallback<List<AggTrade>> callback) {
        getAggTrades(symbol, null, null, null, null, callback);
    }

    @Override
    public void getCandlestickBars(String symbol, CandlestickInterval interval, Integer limit, Long startTime, Long endTime, BlocPalXApiCallback<List<Candlestick>> callback) {
        apiService.getCandlestickBars(symbol, interval.getIntervalId(), limit, startTime, endTime).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getCandlestickBars(String symbol, CandlestickInterval interval, BlocPalXApiCallback<List<Candlestick>> callback) {
        getCandlestickBars(symbol, interval, null, null, null, callback);
    }

    @Override
    public void get24HrPriceStatistics(String symbol, BlocPalXApiCallback<TickerStatistics> callback) {
        apiService.get24HrPriceStatistics(symbol).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getAll24HrPriceStatistics(BlocPalXApiCallback<List<TickerStatistics>> callback) {
        apiService.getAll24HrPriceStatistics().enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getAllPrices(BlocPalXApiCallback<List<TickerPrice>> callback) {
        apiService.getLatestPrices().enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getPrice(String symbol, BlocPalXApiCallback<TickerPrice> callback) {
        apiService.getLatestPrice(symbol).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getBookTickers(BlocPalXApiCallback<List<BookTicker>> callback) {
        apiService.getBookTickers().enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getBookTicker(String symbol, BlocPalXApiCallback<BookTicker> callback) {
        apiService.getBookTicker(symbol).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    // Account endpoints

    @Override
    public void newOrder(NewOrder order, BlocPalXApiCallback<NewOrderResponse> callback) {
        apiService.newOrder(order.getSymbol(), order.getSide(), order.getType(),
                order.getTimeInForce(), order.getQuantity(), order.getQuoteOrderQty(), order.getPrice(), order.getNewClientOrderId(), order.getStopPrice(),
                order.getIcebergQty(), order.getNewOrderRespType(), order.getRecvWindow(), order.getTimestamp()).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void newOrderTest(NewOrder order, BlocPalXApiCallback<Void> callback) {
        apiService.newOrderTest(order.getSymbol(), order.getSide(), order.getType(),
                order.getTimeInForce(), order.getQuantity(), order.getPrice(), order.getNewClientOrderId(), order.getStopPrice(),
                order.getIcebergQty(), order.getNewOrderRespType(), order.getRecvWindow(), order.getTimestamp()).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getOrderStatus(OrderStatusRequest orderStatusRequest, BlocPalXApiCallback<Order> callback) {
        apiService.getOrderStatus(orderStatusRequest.getSymbol(),
                orderStatusRequest.getOrderId(), orderStatusRequest.getOrigClientOrderId(),
                orderStatusRequest.getRecvWindow(), orderStatusRequest.getTimestamp()).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void cancelOrder(CancelOrderRequest cancelOrderRequest, BlocPalXApiCallback<CancelOrderResponse> callback) {
        apiService.cancelOrder(cancelOrderRequest.getSymbol(),
                cancelOrderRequest.getOrderId(), cancelOrderRequest.getOrigClientOrderId(), cancelOrderRequest.getNewClientOrderId(),
                cancelOrderRequest.getRecvWindow(), cancelOrderRequest.getTimestamp()).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getOpenOrders(OrderRequest orderRequest, BlocPalXApiCallback<List<Order>> callback) {
        apiService.getOpenOrders(orderRequest.getSymbol(),
                orderRequest.getRecvWindow(), orderRequest.getTimestamp()).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getAllOrders(AllOrdersRequest orderRequest, BlocPalXApiCallback<List<Order>> callback) {
        apiService.getAllOrders(orderRequest.getSymbol(),
                orderRequest.getOrderId(), orderRequest.getLimit(),
                orderRequest.getRecvWindow(), orderRequest.getTimestamp()).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getAccount(Long recvWindow, Long timestamp, BlocPalXApiCallback<Account> callback) {
        apiService.getAccount(recvWindow, timestamp).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getAccount(BlocPalXApiCallback<Account> callback) {
        long timestamp = System.currentTimeMillis();
        apiService.getAccount(BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, timestamp).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getMyTrades(String symbol, Integer limit, Long fromId, Long recvWindow, Long timestamp, BlocPalXApiCallback<List<Trade>> callback) {
        apiService.getMyTrades(symbol, limit, fromId, recvWindow, timestamp).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getMyTrades(String symbol, Integer limit, BlocPalXApiCallback<List<Trade>> callback) {
        getMyTrades(symbol, limit, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis(), callback);
    }

    @Override
    public void getMyTrades(String symbol, BlocPalXApiCallback<List<Trade>> callback) {
        getMyTrades(symbol, null, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis(), callback);
    }

    // wallet endpoints

    @Override
    public void getAllAssets(BlocPalXApiCallback<List<UserAsset>> callback) {
        apiService.getAllAssets(BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void withdraw(String asset, String address, String amount, String name, String addressTag, BlocPalXApiCallback<WithdrawResult> callback) {
        apiService.withdraw(asset, address, amount, name, addressTag, "", null, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis())
                .enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void withdraw(String asset, String address, String amount, String name, String addressTag, String code, BlocPalXApiCallback<WithdrawResult> callback) {
        apiService.withdraw(asset, address, amount, name, addressTag, code, null, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis())
                .enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void withdraw(String asset, String address, String amount, String name, String addressTag, String code, String network, String withdrawOrderId, BlocPalXApiCallback<WithdrawResult> callback) {
        apiService.withdraw(asset, address, amount, name, addressTag, code, network, withdrawOrderId, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis())
                .enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getDepositHistory(String asset, BlocPalXApiCallback<DepositHistory> callback) {
        apiService.getDepositHistory(asset, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis())
                .enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getWithdrawHistory(String asset, BlocPalXApiCallback<WithdrawHistory> callback) {
        apiService.getWithdrawHistory(asset, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis())
                .enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getDepositAddress(String asset, BlocPalXApiCallback<DepositAddress> callback) {
        apiService.getDepositAddress(asset, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis())
                .enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void getDepositAddress(String asset, String network, BlocPalXApiCallback<DepositAddress> callback) {
        apiService.getDepositAddress(asset, network, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis())
                .enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    // User stream endpoints

    @Override
    public void startUserDataStream(BlocPalXApiCallback<ListenKey> callback) {
        apiService.startUserDataStream().enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void keepAliveUserDataStream(String listenKey, BlocPalXApiCallback<Void> callback) {
        apiService.keepAliveUserDataStream(listenKey).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public void closeUserDataStream(String listenKey, BlocPalXApiCallback<Void> callback) {
        apiService.closeAliveUserDataStream(listenKey).enqueue(new BlocPalXApiCallbackAdapter<>(callback));
    }

    @Override
    public String getBaseUrl() {
        return baseUrl;
    }
}
