package com.blocpalx.api.client.impl;

import com.blocpalx.api.client.BlocPalXApiCallback;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import com.blocpalx.api.client.exception.BlocPalXApiException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

import java.io.IOException;

/**
 * BlocPalX API WebSocket listener.
 */
public class BlocPalXApiWebSocketListener<T> extends WebSocketListener {

    private BlocPalXApiCallback<T> callback;

    private static final ObjectMapper mapper = new ObjectMapper();

    private final ObjectReader objectReader;

    private boolean closing = false;

    public BlocPalXApiWebSocketListener(BlocPalXApiCallback<T> callback, Class<T> eventClass) {
        this.callback = callback;
        this.objectReader = mapper.readerFor(eventClass);
    }

    public BlocPalXApiWebSocketListener(BlocPalXApiCallback<T> callback, TypeReference<T> eventTypeReference) {
        this.callback = callback;
        this.objectReader = mapper.readerFor(eventTypeReference);
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        try {
            if (BlocPalXApiConstants.OUTPUT_SERVER_RESPONSES) {
                System.out.println("\n  raw text: " + text + "\n");
            }
            T event = objectReader.readValue(text);
            callback.onResponse(event);
        } catch (IOException e) {
            throw new BlocPalXApiException(e);
        }
    }

    @Override
    public void onClosing(final WebSocket webSocket, final int code, final String reason) {
        closing = true;
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        if (!closing) {
            callback.onFailure(t);
        }
    }
}