package com.blocpalx.api.client.impl;

import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.account.*;
import com.blocpalx.api.client.domain.account.request.*;
import com.blocpalx.api.client.domain.general.ExchangeInfo;
import com.blocpalx.api.client.domain.general.UserAsset;
import com.blocpalx.api.client.domain.market.*;
import com.blocpalx.api.client.domain.market.request.OrderBookDepthLimit;

import java.util.List;

import static com.blocpalx.api.client.impl.BlocPalXApiServiceGenerator.*;

public class BlocPalXApiRestClientImpl implements BlocPalXApiRestClient {

    private final BlocPalXApiService apiService;
    private final BlocPalXEnvironment env;
    private final String baseUrl;

    public BlocPalXApiRestClientImpl(BlocPalXEnvironment env, String apiKey, String secret) {
        this.apiService = createService(BlocPalXApiService.class, env, apiKey, secret);
        this.env = env;
        this.baseUrl = getBaseUrlByEnvironment(env);
    }

    // General endpoints

    @Override
    public void ping() {
        executeSync(apiService.ping());
    }

    @Override
    public Long getServerTime() {
        return executeSync(apiService.getServerTime()).getServerTime();
    }

    @Override
    public ExchangeInfo getExchangeInfo() {
        return executeSync(apiService.getExchangeInfo());
    }

    // Market Data endpoints

    @Override
    public OrderBook getOrderBook(String symbol, OrderBookDepthLimit limit) {
        return executeSync(apiService.getOrderBook(symbol, limit.getLimit()));
    }

    @Override
    public List<TradeHistoryItem> getTrades(String symbol, Integer limit) {
        return executeSync(apiService.getTrades(symbol, limit));
    }

    @Override
    public List<TradeHistoryItem> getHistoricalTrades(String symbol, Integer limit, Long fromId) {
        return executeSync(apiService.getHistoricalTrades(symbol, limit, fromId));
    }

    @Override
    public List<AggTrade> getAggTrades(String symbol, String fromId, Integer limit, Long startTime, Long endTime) {
        return executeSync(apiService.getAggTrades(symbol, fromId, limit, startTime, endTime));
    }

    @Override
    public List<AggTrade> getAggTrades(String symbol) {
        return getAggTrades(symbol, null, null, null, null);
    }

    @Override
    public List<Candlestick> getCandlestickBars(String symbol, CandlestickInterval interval, Integer limit, Long startTime, Long endTime) {
        return executeSync(apiService.getCandlestickBars(symbol, interval.getIntervalId(), limit, startTime, endTime));
    }

    @Override
    public List<Candlestick> getCandlestickBars(String symbol, CandlestickInterval interval) {
        return getCandlestickBars(symbol, interval, null, null, null);
    }

    @Override
    public TickerStatistics get24HrPriceStatistics(String symbol) {
        return executeSync(apiService.get24HrPriceStatistics(symbol));
    }

    @Override
    public List<TickerStatistics> getAll24HrPriceStatistics() {
        return executeSync(apiService.getAll24HrPriceStatistics());
    }

    @Override
    public TickerPrice getPrice(String symbol) {
        return executeSync(apiService.getLatestPrice(symbol));
    }

    @Override
    public List<TickerPrice> getAllPrices() {
        return executeSync(apiService.getLatestPrices());
    }

    @Override
    public List<BookTicker> getBookTickers() {
        return executeSync(apiService.getBookTickers());
    }

    @Override
    public BookTicker getBookTicker(String symbol) {
        return executeSync(apiService.getBookTicker(symbol));
    }


    // Account endpoints

    @Override
    public NewOrderResponse newOrder(NewOrder order) {
        return executeSync(apiService.newOrder(order.getSymbol(), order.getSide(), order.getType(),
                order.getTimeInForce(), order.getQuantity(), order.getQuoteOrderQty(), order.getPrice(), order.getNewClientOrderId(),
                order.getStopPrice(), order.getIcebergQty(), order.getNewOrderRespType(), order.getRecvWindow(), order.getTimestamp()));
    }

    @Override
    public void newOrderTest(NewOrder order) {
        executeSync(apiService.newOrderTest(order.getSymbol(), order.getSide(), order.getType(),
                order.getTimeInForce(), order.getQuantity(), order.getPrice(), order.getNewClientOrderId(), order.getStopPrice(),
                order.getIcebergQty(), order.getNewOrderRespType(), order.getRecvWindow(), order.getTimestamp()));
    }

    @Override
    public Order getOrderStatus(OrderStatusRequest orderStatusRequest) {
        return executeSync(apiService.getOrderStatus(orderStatusRequest.getSymbol(),
                orderStatusRequest.getOrderId(), orderStatusRequest.getOrigClientOrderId(),
                orderStatusRequest.getRecvWindow(), orderStatusRequest.getTimestamp()));
    }

    @Override
    public CancelOrderResponse cancelOrder(CancelOrderRequest cancelOrderRequest) {
        return executeSync(apiService.cancelOrder(cancelOrderRequest.getSymbol(),
                cancelOrderRequest.getOrderId(), cancelOrderRequest.getOrigClientOrderId(), cancelOrderRequest.getNewClientOrderId(),
                cancelOrderRequest.getRecvWindow(), cancelOrderRequest.getTimestamp()));
    }

    @Override
    public List<Order> getOpenOrders(OrderRequest orderRequest) {
        return executeSync(apiService.getOpenOrders(orderRequest.getSymbol(), orderRequest.getRecvWindow(), orderRequest.getTimestamp()));
    }

    @Override
    public List<Order> getAllOrders(AllOrdersRequest orderRequest) {
        return executeSync(apiService.getAllOrders(orderRequest.getSymbol(),
                orderRequest.getOrderId(), orderRequest.getLimit(),
                orderRequest.getRecvWindow(), orderRequest.getTimestamp()));
    }

    @Override
    public Account getAccount(Long recvWindow, Long timestamp) {
        return executeSync(apiService.getAccount(recvWindow, timestamp));
    }

    @Override
    public Account getAccount() {
        return getAccount(BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis());
    }

    @Override
    public List<Trade> getMyTrades(String symbol, Integer limit, Long fromId, Long recvWindow, Long timestamp) {
        return executeSync(apiService.getMyTrades(symbol, limit, fromId, recvWindow, timestamp));
    }

    @Override
    public List<Trade> getMyTrades(String symbol, Integer limit) {
        return getMyTrades(symbol, limit, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis());
    }

    @Override
    public List<Trade> getMyTrades(String symbol) {
        return getMyTrades(symbol, null, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis());
    }

    // wallet endpoints

    @Override
    public List<UserAsset> getAllAssets() {
        return executeSync(apiService.getAllAssets(BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

    @Override
    public WithdrawResult withdraw(String asset, String address, String amount, String name, String addressTag) {
        return executeSync(apiService.withdraw(asset, address, amount, name, addressTag, "", null, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

    @Override
    public WithdrawResult withdraw(String asset, String address, String amount, String name, String addressTag, String code) {
        return executeSync(apiService.withdraw(asset, address, amount, name, addressTag, code, null, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

    @Override
    public WithdrawResult withdraw(String asset, String address, String amount, String name, String addressTag, String code, String network, String withdrawOrderId) {
        return executeSync(apiService.withdraw(asset, address, amount, name, addressTag, code, network, withdrawOrderId, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

    @Override
    public DepositHistory getDepositHistory(String asset) {
        return executeSync(apiService.getDepositHistory(asset, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

    @Override
    public WithdrawHistory getWithdrawHistory(String asset) {
        return executeSync(apiService.getWithdrawHistory(asset, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

    @Override
    public DepositAddress getDepositAddress(String asset) {
        return executeSync(apiService.getDepositAddress(asset, null, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

    @Override
    public DepositAddress getDepositAddress(String asset, String network) {
        return executeSync(apiService.getDepositAddress(asset, network, BlocPalXApiConstants.DEFAULT_RECEIVING_WINDOW, System.currentTimeMillis()));
    }

    // User stream endpoints

    @Override
    public String startUserDataStream() {
        return executeSync(apiService.startUserDataStream()).toString();
    }

    @Override
    public void keepAliveUserDataStream(String listenKey) {
        executeSync(apiService.keepAliveUserDataStream(listenKey));
    }

    @Override
    public void closeUserDataStream(String listenKey) {
        executeSync(apiService.closeAliveUserDataStream(listenKey));
    }

    @Override
    public String getBaseUrl() {
        return baseUrl;
    }

}
