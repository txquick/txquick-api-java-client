package com.blocpalx.api.client;

import com.blocpalx.api.client.domain.event.*;
import com.blocpalx.api.client.domain.market.CandlestickInterval;

import java.io.Closeable;
import java.util.List;

/**
 * BlocPalX API data streaming facade, supporting streaming of events through web sockets.
 */
public interface BlocPalXApiWebSocketClient extends Closeable {

    /**
     * Open a new web socket to receive {@link DiffDepthEvent depthEvents} on a callback.
     *
     * @param symbols  market (one or coma-separated) symbol(s) to subscribe to
     * @param callback the callback to call on new events
     * @return a {@link Closeable} that allows the underlying web socket to be closed.
     */
    Closeable onDiffDepthEvent(String symbols, BlocPalXApiCallback<DiffDepthEvent> callback);

    /**
     * Open a new web socket to receive {@link DiffDepthEvent depthEvents} on a callback.
     *
     * @param symbols  market (one or coma-separated) symbol(s) to subscribe to
     * @param levels   depth levels (5, 10, or 20)
     * @param callback the callback to call on new events
     * @return a {@link Closeable} that allows the underlying web socket to be closed.
     */
    Closeable onPartialDepthEvent(String symbols, int levels, BlocPalXApiCallback<PartialDepthEvent> callback);

    /**
     * Open a new web socket to receive {@link CandlestickEvent candlestickEvents} on a callback.
     *
     * @param symbols  market (one or coma-separated) symbol(s) to subscribe to
     * @param interval the interval of the candles tick events required
     * @param callback the callback to call on new events
     * @return a {@link Closeable} that allows the underlying web socket to be closed.
     */
    Closeable onCandlestickEvent(String symbols, CandlestickInterval interval, BlocPalXApiCallback<CandlestickEvent> callback);

    /**
     * Open a new web socket to receive {@link AggTradeEvent aggTradeEvents} on a callback.
     *
     * @param symbols  market (one or coma-separated) symbol(s) to subscribe to
     * @param callback the callback to call on new events
     * @return a {@link Closeable} that allows the underlying web socket to be closed.
     */
    Closeable onAggTradeEvent(String symbols, BlocPalXApiCallback<AggTradeEvent> callback);

    /**
     * Open a new web socket to receive {@link UserDataUpdateEvent userDataUpdateEvents} on a callback.
     *
     * @param listenKey the listen key to subscribe to.
     * @param callback  the callback to call on new events
     * @return a {@link Closeable} that allows the underlying web socket to be closed.
     */
    Closeable onUserDataUpdateEvent(String listenKey, BlocPalXApiCallback<UserDataUpdateEvent> callback);

    /**
     * Open a new web socket to receive {@link TickerEvent tickerEvents} on a callback.
     *
     * @param symbols  market (one or coma-separated) symbol(s) to subscribe to
     * @param callback the callback to call on new events
     * @return a {@link Closeable} that allows the underlying web socket to be closed.
     */
    Closeable onTickerEvent(String symbols, BlocPalXApiCallback<TickerEvent> callback);

    /**
     * Open a new web socket to receive {@link List<TickerEvent> allMarketTickersEvents} on a callback.
     *
     * @param callback the callback to call on new events
     * @return a {@link Closeable} that allows the underlying web socket to be closed.
     */
    Closeable onAllMarketTickersEvent(BlocPalXApiCallback<List<TickerEvent>> callback);

    /**
     * Open a new web socket to receive {@link BookTickerEvent bookTickerEvents} on a callback.
     *
     * @param symbols  market (one or coma-separated) symbol(s) to subscribe to
     * @param callback the callback to call on new events
     * @return a {@link Closeable} that allows the underlying web socket to be closed.
     */
    Closeable onBookTickerEvent(String symbols, BlocPalXApiCallback<BookTickerEvent> callback);

    /**
     * Open a new web socket to receive {@link BookTickerEvent allBookTickersEvents} on a callback.
     *
     * @param callback the callback to call on new events
     * @return a {@link Closeable} that allows the underlying web socket to be closed.
     */
    Closeable onAllBookTickersEvent(BlocPalXApiCallback<BookTickerEvent> callback);

    /**
     * @deprecated This method is no longer functional. Please use the returned {@link Closeable} from any of the other methods to close the web socket.
     */
    @Deprecated
    void close();

    /**
     * @return base API URL currently in use
     */
    String getBaseUrl();

}
