package com.blocpalx.api.client.exception;

import com.blocpalx.api.client.BlocPalXApiError;

/**
 * An exception which can occur while invoking methods of the BlocPalX API.
 */
public class BlocPalXApiException extends RuntimeException {

    private static final long serialVersionUID = 3788669840036201041L;
    /**
     * Error response object returned by BlocPalX API.
     */
    private BlocPalXApiError error;

    /**
     * Instantiates a new BlocPalX api exception.
     *
     * @param error an error response object
     */
    public BlocPalXApiException(BlocPalXApiError error) {
        this.error = error;
    }

    /**
     * Instantiates a new BlocPalX api exception.
     */
    public BlocPalXApiException() {
        super();
    }

    /**
     * Instantiates a new BlocPalX api exception.
     *
     * @param message the message
     */
    public BlocPalXApiException(String message) {
        super(message);
    }

    /**
     * Instantiates a new BlocPalX api exception.
     *
     * @param cause the cause
     */
    public BlocPalXApiException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new BlocPalX api exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public BlocPalXApiException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @return the response error object from BlocPalX API, or null if no response object was returned (e.g. server returned 500).
     */
    public BlocPalXApiError getError() {
        return error;
    }

    @Override
    public String getMessage() {
        if (error != null) {
            return error.getMsg();
        }
        return super.getMessage();
    }
}
