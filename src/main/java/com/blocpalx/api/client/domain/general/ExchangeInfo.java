package com.blocpalx.api.client.domain.general;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.blocpalx.api.client.exception.BlocPalXApiException;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ExchangeInfo {

    private String timezone;

    private Long serverTime;

//  private List<RateLimit> rateLimits;

    private List<SymbolInfo> symbols;


    /**
     * @param symbol the symbol to obtain information for (e.g. ETHBTC)
     * @return symbol exchange information
     */
    public SymbolInfo getSymbolInfo(String symbol) {
        return symbols.stream().filter(symbolInfo -> symbolInfo.getSymbol().equals(symbol))
                .findFirst()
                .orElseThrow(() -> new BlocPalXApiException("Unable to obtain information for symbol " + symbol));
    }

}
