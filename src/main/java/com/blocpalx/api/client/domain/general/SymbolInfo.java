package com.blocpalx.api.client.domain.general;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.blocpalx.api.client.domain.OrderType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SymbolInfo {

    private String symbol;

    private SymbolStatus status;

    private String baseAsset;

    private Integer baseAssetPrecision;

    private String quoteAsset;

    private Integer quotePrecision;

    @JsonProperty("extendedOrderTypes")
    private List<OrderType> orderTypes;

    private boolean icebergAllowed;

    private List<SymbolFilter> filters;

    /**
     * @param filterType filter type to filter for.
     * @return symbol filter information for the provided filter type.
     */
    public SymbolFilter getSymbolFilter(FilterType filterType) {
        return filters.stream()
                .filter(symbolFilter -> symbolFilter.getFilterType() == filterType)
                .findFirst()
                .get();
    }

}
