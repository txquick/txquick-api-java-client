package com.blocpalx.api.client.domain.market.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum OrderBookDepthLimit {
    FIVE(5),
    TEN(10),
    TWENTY(20),
    FIFTY(50),
    ONE_HUNDRED(100),
    FIVE_HUNDRED(500),
    ONE_THOUSAND(1000);

    private final int limit;

    OrderBookDepthLimit(int limit) {
        this.limit = limit;
    }

    public int getLimit() { return limit; }

}
