package com.blocpalx.api.client.domain.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Dummy type to wrap a listen key from a server response.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListenKey {

    private String listenKey;

    @Override
    public String toString() {
        return listenKey;
    }
}
