package com.blocpalx.api.client.domain.market;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)

@Data
public class TickerPrice {
    /**
     * Ticker symbol.
     */
    private String symbol;

    /**
     * Latest price.
     */
    private String price;

}
