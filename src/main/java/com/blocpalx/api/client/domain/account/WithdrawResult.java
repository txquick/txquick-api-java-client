package com.blocpalx.api.client.domain.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * A withdraw result that was done to a BlocPalX account.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WithdrawResult {

    /**
     * Withdraw message.
     */
    @Deprecated
    private String msg;

    /**
     * Withdraw success.
     */
    @Deprecated
    private boolean success;

    /**
     * Withdraw id.
     */
    private String id;

    @Deprecated
    public String getMsg() {
        return msg;
    }

    @Deprecated
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Deprecated
    public boolean isSuccess() {
        return success;
    }

    @Deprecated
    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("id", id)
                .toString();
    }


}
