package com.blocpalx.api.client.domain.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Represents the best price/qty on the order book for a given symbol.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookTickerEvent {

    @JsonProperty("u")
    private Long updateId; // order book updateId

    @JsonProperty("s")
    private String symbol; // symbol

    @JsonProperty("b")
    private String bestBidPrice; // best bid price

    @JsonProperty("B")
    private String bestBidQty; // best bid qty

    @JsonProperty("a")
    private String bestAskPrice; // best ask price

    @JsonProperty("A")
    private String bestAskQty; // best ask qty

    @Override
    public String toString() {
        return new ToStringBuilder(this, BlocPalXApiConstants.TO_STRING_BUILDER_STYLE)
                .append("updateId", updateId)
                .append("symbol", symbol)
                .append("bestBidPrice", bestBidPrice)
                .append("bestBidQty", bestBidQty)
                .append("bestAskPrice", bestAskPrice)
                .append("bestAskQty", bestAskQty)
                .toString();
    }
}
