package com.blocpalx.api.client.domain.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class TradeHistoryItem {

    /**
     * Trade id.
     */
    private long id;

    /**
     * Price.
     */
    private String price;

    /**
     * Quantity.
     */
    private String qty;

    /**
     * Trade execution time.
     */
    private long time;

    /**
     * Is buyer maker ?
     */
    @JsonProperty("isBuyerMaker")
    private boolean isBuyerMaker;

    /**
     * Is best match ?
     */
    @JsonProperty("isBestMatch")
    private boolean isBestMatch;

}
