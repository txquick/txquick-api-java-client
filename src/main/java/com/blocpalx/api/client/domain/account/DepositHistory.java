package com.blocpalx.api.client.domain.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * History of account deposits
 *
 * @see Deposit
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DepositHistory {

    @JsonProperty("depositList")
    private List<Deposit> depositList;

    @Deprecated
    private boolean success;

    @Deprecated
    private String msg;

    @Deprecated
    public String getMsg() {
        return msg;
    }

    @Deprecated
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Deposit> getDepositList() {
        return depositList;
    }

    public void setDepositList(List<Deposit> depositList) {
        this.depositList = depositList;
    }

    @Deprecated
    public boolean isSuccess() {
        return success;
    }

    @Deprecated
    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, BlocPalXApiConstants.TO_STRING_BUILDER_STYLE)
                .append("depositList", depositList)
                .append("success", success)
                .append("msg", msg)
                .toString();
    }
}
