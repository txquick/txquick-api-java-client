package com.blocpalx.api.client.domain.market;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class BookTicker {

    /**
     * Ticker symbol.
     */
    private String symbol;

    /**
     * Bid price.
     */
    private String bidPrice;

    /**
     * Bid quantity
     */
    private String bidQty;

    /**
     * Ask price.
     */
    private String askPrice;

    /**
     * Ask quantity.
     */
    private String askQty;

}
