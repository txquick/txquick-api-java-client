package com.blocpalx.api.client.domain.market;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class OrderBook {

    private long lastUpdateId;

    /**
     * List of bids (price/qty).
     */
    private List<OrderBookEntry> bids;

    /**
     * List of asks (price/qty).
     */
    private List<OrderBookEntry> asks;

}
