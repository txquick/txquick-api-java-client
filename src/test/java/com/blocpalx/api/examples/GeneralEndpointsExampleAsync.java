package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiAsyncRestClient;
import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.general.FilterType;
import com.blocpalx.api.client.domain.general.SymbolFilter;
import com.blocpalx.api.client.domain.general.SymbolInfo;

/**
 * Examples on how to use the general endpoints.
 */
public class GeneralEndpointsExampleAsync {

    public static void main(String[] args) throws InterruptedException {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(BlocPalXEnvironment.SANDBOX);
        BlocPalXApiAsyncRestClient client = factory.newAsyncRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Test connectivity
        client.ping(response -> System.out.println("Ping succeeded."));

        // Check server time
        client.getServerTime(response -> System.out.println(response.getServerTime()));

        // Exchange info
        client.getExchangeInfo(exchangeInfo -> {
            System.out.println(exchangeInfo.getTimezone());
            System.out.println(exchangeInfo.getSymbols());

            // Obtain symbol information
            SymbolInfo symbolInfo = exchangeInfo.getSymbolInfo("XMRBTC");
            System.out.println(symbolInfo.getStatus());

            SymbolFilter priceFilter = symbolInfo.getSymbolFilter(FilterType.PRICE_FILTER);
            System.out.println(priceFilter.getMinPrice());
            System.out.println(priceFilter.getTickSize());
        });

    }
}
