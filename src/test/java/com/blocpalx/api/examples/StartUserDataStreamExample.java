package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.BlocPalXApiWebSocketClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.event.AccountUpdateEvent;
import com.blocpalx.api.client.domain.event.OrderTradeUpdateEvent;
import com.blocpalx.api.client.domain.event.UserDataUpdateEvent.UserDataUpdateEventType;

import java.util.Date;

/**
 * User data stream endpoints examples.
 * <p>
 * It illustrates how to create a stream to obtain updates on a user account,
 * as well as update on trades/orders on a user account.
 */
public class StartUserDataStreamExample {

    public static void main(String[] args) {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(
                BlocPalXEnvironment.SANDBOX,
                ApiKeysForExamples.API_KEY,
                ApiKeysForExamples.API_SECRET);
        BlocPalXApiRestClient client = factory.newRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // First, we obtain a listenKey which is required to interact with the user data stream
        String listenKey = client.startUserDataStream();
        // String listenKey = "Qh96j5jQgy5SKsHlEDwosaaS8hetjowUN8woUxSCgzpCTBY1ogF6x7Pd0knP6gqWB";

        System.out.println("\n********** LISTEN KEY **********\n");
        System.out.println(listenKey);
        System.out.println("\n********************************\n");

        // Then, we open a new web socket client, and provide a callback that is called on every update
        BlocPalXApiWebSocketClient webSocketClient = factory.newWebSocketClient();

        // Listen for changes in the account
        webSocketClient.onUserDataUpdateEvent(listenKey, response -> {
            if (response.getEventType() == UserDataUpdateEventType.ACCOUNT_UPDATE) {
                AccountUpdateEvent accountUpdateEvent = response.getAccountUpdateEvent();
                // Print new balances of every available asset
                System.out.println(accountUpdateEvent.getBalances());
            } else if (response.getEventType() == UserDataUpdateEventType.ACCOUNT_POSITION_UPDATE) {
                AccountUpdateEvent accountUpdateEvent = response.getAccountUpdateEvent();
                // Print new balances of every available asset
                System.out.println(accountUpdateEvent.getBalances());
            } else {
                OrderTradeUpdateEvent orderTradeUpdateEvent = response.getOrderTradeUpdateEvent();
                // Print details about an order/trade
                System.out.println("Order Event: " + orderTradeUpdateEvent);

                // Print status
                System.out.println("Execution Type: " + orderTradeUpdateEvent.getExecutionType());

                // Print original quantity
                System.out.println("Quantity: " + orderTradeUpdateEvent.getOriginalQuantity());

                // Or price
                System.out.println("Price: " + orderTradeUpdateEvent.getPrice());
            }
        });
        System.out.println("Waiting for events...");

        try {
            while (true) {
                // We can keep alive the user data stream
                Thread.sleep(5 * 60 * 1000); // every 5 minutes
                System.out.println(new Date() + ": Extending session with PUT call");
                client.keepAliveUserDataStream(listenKey);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Or we can invalidate it, whenever it is no longer needed
        // client.closeUserDataStream(listenKey);
    }
}
