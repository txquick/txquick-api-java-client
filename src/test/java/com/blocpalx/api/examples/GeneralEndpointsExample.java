package com.blocpalx.api.examples;

import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.general.*;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiRestClient;

import java.util.List;

public class GeneralEndpointsExample {

    public static void main(String[] args) {

        PrettyPrint prettyPrint = new PrettyPrint();

        BlocPalXApiClientFactory apiClientFactory = BlocPalXApiClientFactory.newInstance(BlocPalXEnvironment.SANDBOX);
        BlocPalXApiRestClient client = apiClientFactory.newRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Test connectivity
        client.ping();

        // Check server time
        long serverTime = client.getServerTime();
        System.out.println(serverTime);

        // Exchange info
        ExchangeInfo exchangeInfo = client.getExchangeInfo();
        System.out.println(exchangeInfo.getTimezone());
        System.out.println(exchangeInfo.getSymbols());
        // prettyPrint.printIt(exchangeInfo.getTimezone());
        // prettyPrint.printIt(exchangeInfo.getSymbols());

        // Obtain symbol information
        SymbolInfo symbolInfo = exchangeInfo.getSymbolInfo("XMRBTC");
        System.out.println(symbolInfo.getStatus());

        SymbolFilter priceFilter = symbolInfo.getSymbolFilter(FilterType.PRICE_FILTER);
        System.out.println(priceFilter.getMinPrice());
        System.out.println(priceFilter.getTickSize());

    }
}
