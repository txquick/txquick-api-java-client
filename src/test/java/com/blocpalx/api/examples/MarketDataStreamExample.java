package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiWebSocketClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.market.CandlestickInterval;

import java.io.IOException;

/**
 * Market data stream endpoints examples.
 * <p>
 * It illustrates how to create a stream to obtain updates on market data such as executed trades.
 */
public class MarketDataStreamExample {

    public static void main(String[] args) throws InterruptedException, IOException {
        BlocPalXApiWebSocketClient client = BlocPalXApiClientFactory.newInstance(BlocPalXEnvironment.SANDBOX).newWebSocketClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Obtain 5m candlesticks in real-time for XMR/BTC
        client.onCandlestickEvent("XMRBTC", CandlestickInterval.FIVE_MINUTES, response -> System.out.println(response));

        // Listen for aggregated trade events for XMR/BTC
        // client.onAggTradeEvent("XMRBTC", response -> System.out.println(response));

        // Get the full tops of the order book at 1s intervals
        // client.onPartialDepthEvent("xmrbtc", 10, response -> System.out.println(response));

        // Listen for changes in the best order book price in XMR/BTC
        // client.onBookTickerEvent("xmrbtc", response -> System.out.println(response));

        // Listen for changes in the order book in XMR/BTC
        // client.onDiffDepthEvent("xmrbtc", response -> System.out.println(response));

        // listen on multiple comma-separated pairs
        // client.onDiffDepthEvent("xmrbtc, xrpbtc, ethbtc", response -> System.out.println(response));

        // Listen for trade events for XMR/BTC
        // (WIP: not yet supported in client... use onAggTradeEvent())
        // client.onTradeEvent("XMRBTC", response -> System.out.println(response));

    }
}
