package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiCallback;
import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.BlocPalXApiWebSocketClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.event.*;
import com.blocpalx.api.client.domain.event.UserDataUpdateEvent.UserDataUpdateEventType;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

/**
 * User data stream endpoints examples.
 * <p>
 * It illustrates how to create a stream to obtain updates on a user account,
 * as well as update on trades/orders on a user account.
 */
public class UserDataStreamExample {

    private final BlocPalXApiRestClient restClient;
    private final BlocPalXApiWebSocketClient wsClient;
    private final WsCallback wsCallback = new WsCallback();

    private volatile Closeable webSocket;

    private String listenKey;

    public UserDataStreamExample() {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(
                BlocPalXEnvironment.SANDBOX,
                ApiKeysForExamples.API_KEY,
                ApiKeysForExamples.API_SECRET);
        this.wsClient = factory.newWebSocketClient();
        this.restClient = factory.newRestClient();

        initialize();
    }

    private void initialize() {
        System.out.println("Base WS URL is: " + wsClient.getBaseUrl());
        System.out.println("Base REST URL is: " + restClient.getBaseUrl());

        // First, we obtain a listenKey which is required to interact with the user data stream
        listenKey = restClient.startUserDataStream();
        System.out.println("ListenKey is: " + listenKey);

        this.webSocket = wsClient.onUserDataUpdateEvent(listenKey, wsCallback);

        final Consumer<UserDataUpdateEvent> userDataUpdates = newEvent -> {
            if (newEvent.getEventType() == UserDataUpdateEventType.ACCOUNT_UPDATE) {
                AccountUpdateEvent accountUpdateEvent = newEvent.getAccountUpdateEvent();
                // Print new balances of every available asset
                System.out.println(accountUpdateEvent.getBalances());
            } else if (newEvent.getEventType() == UserDataUpdateEventType.ACCOUNT_POSITION_UPDATE) {
                AccountUpdateEvent accountUpdateEvent = newEvent.getAccountUpdateEvent();
                // Print new balances of every available asset
                System.out.println(accountUpdateEvent.getBalances());
            } else {
                OrderTradeUpdateEvent orderTradeUpdateEvent = newEvent.getOrderTradeUpdateEvent();
                // Print details about an order/trade
                System.out.println("Order Event: " + orderTradeUpdateEvent);

                // Print status
                System.out.println("Execution Type: " + orderTradeUpdateEvent.getExecutionType());

                // Print original quantity
                System.out.println("Quantity: " + orderTradeUpdateEvent.getOriginalQuantity());

                // Or price
                System.out.println("Price: " + orderTradeUpdateEvent.getPrice());
            }
        };

        wsCallback.setHandler(userDataUpdates);

        System.out.println("Waiting for events...");

        // We can keep alive the user data stream via a scheduled keepalive
        // restClient.keepAliveUserDataStream(listenKey);

        // Or we can invalidate it, whenever it is no longer needed
        // restClient.closeUserDataStream(listenKey);
    }

    public void close() throws IOException {
        webSocket.close();
    }

    public static void main(String[] args) {
        new UserDataStreamExample();
    }

    private final class WsCallback implements BlocPalXApiCallback<UserDataUpdateEvent> {

        private final AtomicReference<Consumer<UserDataUpdateEvent>> handler = new AtomicReference<>();

        @Override
        public void onResponse(final UserDataUpdateEvent response) {
            try {
                handler.get().accept(response);
            } catch (final Exception e) {
                System.err.println("Exception caught processing event");
                e.printStackTrace(System.err);
            }
        }

        @Override
        public void onFailure(Throwable cause) {
            System.out.println("WS connection failed. Reconnecting. cause:" + cause.toString());
            System.out.println("WS connection failed. Reconnecting. message:" + cause.getMessage());
            try {
                Thread.sleep(10000);
                initialize();
            } catch (InterruptedException ex) {
                // do nothing...
            }
        }

        private void setHandler(final Consumer<UserDataUpdateEvent> handler) {
            this.handler.set(handler);
        }
    }

}
