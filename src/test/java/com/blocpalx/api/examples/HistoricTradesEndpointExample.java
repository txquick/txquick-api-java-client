package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.account.TradeHistoryItem;

import java.util.List;

/**
 * Examples on how to get account information.
 */
public class HistoricTradesEndpointExample {

    public static void main(String[] args) {
        PrettyPrint prettyPrint = new PrettyPrint();

        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(
                BlocPalXEnvironment.SANDBOX,
                ApiKeysForExamples.API_KEY,
                "");
        BlocPalXApiRestClient client = factory.newRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Get list of historic trades
        List<TradeHistoryItem> trades = client.getHistoricalTrades("XMRBTC", 10, 400000L);
        System.out.println(trades);
        prettyPrint.printIt(trades);
    }
}
