package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiAsyncRestClient;
import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.TimeInForce;
import com.blocpalx.api.client.domain.account.NewOrder;
import com.blocpalx.api.client.domain.account.NewOrderResponseType;
import com.blocpalx.api.client.domain.account.request.AllOrdersRequest;
import com.blocpalx.api.client.domain.account.request.CancelOrderRequest;
import com.blocpalx.api.client.domain.account.request.OrderRequest;
import com.blocpalx.api.client.domain.account.request.OrderStatusRequest;

import static com.blocpalx.api.client.domain.account.NewOrder.limitBuy;
import static com.blocpalx.api.client.domain.account.NewOrder.marketBuy;

/**
 * Examples on how to place orders, cancel them, and query account information.
 */
public class OrdersExampleAsync {

    public static void main(String[] args) {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(
                BlocPalXEnvironment.SANDBOX,
                ApiKeysForExamples.API_KEY,
                ApiKeysForExamples.API_SECRET);
        BlocPalXApiAsyncRestClient client = factory.newAsyncRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Getting list of open orders
        client.getOpenOrders(new OrderRequest("XMRBTC"), response -> System.out.println(response));

        // Get status of a particular order
        client.getOrderStatus(new OrderStatusRequest("XMRBTC", 745262L),
                response -> System.out.println(response));

        // Getting list of all orders with a limit of 10
        client.getAllOrders(new AllOrdersRequest("XMRBTC").limit(10), response -> System.out.println(response));

        // Placing a test LIMIT order
        client.newOrderTest(NewOrder.limitBuy("XMRBTC", TimeInForce.GTC, "10", "0.1"),
                response -> System.out.println("Test order has succeeded."));

        // Placing a test MARKET order
        client.newOrderTest(NewOrder.marketBuy("XMRBTC", "0.001"), response -> System.out.println("Test order has succeeded."));

        // Placing a real LIMIT order
        client.newOrder(NewOrder.limitBuy("XMRBTC", TimeInForce.GTC, "0.001", "0.0076781").newOrderRespType(NewOrderResponseType.FULL),
                response -> {
                    System.out.println("Created: " + response);

                    // Canceling an order
                    client.cancelOrder(new CancelOrderRequest("XMRBTC", response.getClientOrderId()),
                            cancelResponse -> System.out.println("Cancelled: " + response));
                });
    }
}
