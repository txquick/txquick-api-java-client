package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiAsyncRestClient;
import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.account.Account;

/**
 * Examples on how to get account information.
 */
public class AccountEndpointsExampleAsync {

    public static void main(String[] args) {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(
                BlocPalXEnvironment.SANDBOX,
                ApiKeysForExamples.API_KEY,
                ApiKeysForExamples.API_SECRET);
        BlocPalXApiAsyncRestClient client = factory.newAsyncRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Get accessible asset information
        client.getAllAssets(allAssets ->
                System.out.println(allAssets.stream().filter(asset -> asset.getCoin().equals("BTC")).findFirst().get()));

        // Get account balances (async)
        client.getAccount((Account response) -> System.out.println(response.getAssetBalance("BTC")));

        // Get deposit history (async)
        client.getDepositHistory("BTC", response -> System.out.println(response));

        // Get list of trades (async)
        client.getMyTrades("ETHBTC", response -> System.out.println(response));

        // Withdraw (async)
        /*
        client.withdraw("XRP", "r3BoVWNGT7j6hZjHKQL4DwoW2bCwMDfZSb", "10.0", null, "abc123", response -> {
        });
        */

        // Get withdraw history (async)
        client.getWithdrawHistory("BTC", response -> System.out.println(response));
    }
}
