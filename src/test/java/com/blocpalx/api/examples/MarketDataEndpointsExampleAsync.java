package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiAsyncRestClient;
import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.account.Order;
import com.blocpalx.api.client.domain.market.*;
import com.blocpalx.api.client.domain.market.request.OrderBookDepthLimit;
import com.blocpalx.api.client.exception.BlocPalXApiException;

import java.util.List;

/**
 * Examples on how to get market data information such as the latest price of a symbol, etc., in an async way.
 */
public class MarketDataEndpointsExampleAsync {

    public static void main(String[] args) {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(BlocPalXEnvironment.SANDBOX);
        BlocPalXApiAsyncRestClient client = factory.newAsyncRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Getting depth of a symbol (async)
        client.getOrderBook("XMRBTC", OrderBookDepthLimit.TEN, (OrderBook response) -> {
            System.out.println(response.getBids());
        });

        // Getting latest price of a symbol (async)
        client.get24HrPriceStatistics("XMRBTC", (TickerStatistics response) -> {
            System.out.println(response);
        });

        // Getting all latest prices (async)
        client.getAllPrices((List<TickerPrice> response) -> {
            System.out.println(response);
        });

        // Getting agg trades (async)
        client.getAggTrades("XMRBTC", (List<AggTrade> response) -> System.out.println(response));

        // Weekly candlestick bars for a symbol
        client.getCandlestickBars("XMRBTC", CandlestickInterval.WEEKLY,
                (List<Candlestick> response) -> System.out.println(response));

        // Book tickers (async)
        client.getBookTickers(response -> System.out.println(response));

        // Exception handling
        try {
            client.getOrderBook("UNKNOWN", OrderBookDepthLimit.TEN, response -> System.out.println(response));
        } catch (BlocPalXApiException e) {
            System.out.println(e.getError().getCode()); // -1121
            System.out.println(e.getError().getMsg());  // Invalid symbol
        }
    }
}
