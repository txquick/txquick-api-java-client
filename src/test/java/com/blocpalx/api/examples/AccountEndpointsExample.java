package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.account.*;
import com.blocpalx.api.client.domain.general.UserAsset;

import java.util.List;

/**
 * Examples on how to get account information.
 */
public class AccountEndpointsExample {

    public static void main(String[] args) {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(
                BlocPalXEnvironment.SANDBOX,
                ApiKeysForExamples.API_KEY,
                ApiKeysForExamples.API_SECRET);
        BlocPalXApiRestClient client = factory.newRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Get accessible asset information
        List<UserAsset> allAssets = client.getAllAssets();
        System.out.println(allAssets);
        System.out.println(allAssets.stream().filter(asset -> asset.getCoin().equals("BTC")).findFirst().get());

        // Get account balances
        Account account = client.getAccount();
        System.out.println(account.getBalances());
        System.out.println(account.getAssetBalance("BTC"));

        // Get a new deposit address
        System.out.println(client.getDepositAddress("BTC"));

        // Get deposit history
        System.out.println(client.getDepositHistory("BTC"));

        // Get list of trades
        List<Trade> myTrades = client.getMyTrades("ETHBTC");
        System.out.println(myTrades);

        // Withdraw
        /*
        WithdrawResult withdrawResult = client.withdraw(
                "BTC",
                "tb1ql7w62elx9ucw4pj5lgw4l028hmuw80sndtntxt",
                "0.00003",
                "API Test",
                null,
                null,  // google auth code - required if API WITHDRAW is 2FA secured in the BlocPalX UI
                null,  // optional
                "test-" + UUID.randomUUID()  // optional
        );
        System.out.println(withdrawResult);
        */

        // Get withdraw history
        System.out.println(client.getWithdrawHistory("BTC"));
    }
}
